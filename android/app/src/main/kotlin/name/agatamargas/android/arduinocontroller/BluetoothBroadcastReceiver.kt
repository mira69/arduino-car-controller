package name.agatamargas.android.arduinocontroller

import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class BluetoothBroadcastReceiver(private val listener: BluetoothBRListener) : BroadcastReceiver() {
	constructor () : this(object : BluetoothBRListener {})

	override fun onReceive (context: Context, intent: Intent): Unit {
		if (intent.action != BluetoothAdapter.ACTION_STATE_CHANGED) {
			return
		}
		when (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1)) {
			BluetoothAdapter.STATE_OFF -> listener.onTurnedOff()
			BluetoothAdapter.STATE_ON -> listener.onTurnedOn()
			BluetoothAdapter.STATE_TURNING_OFF -> listener.onTurningOff()
			BluetoothAdapter.STATE_TURNING_ON -> listener.onTurningOn()
		}
	}
}

interface BluetoothBRListener {
	fun onTurnedOff (): Unit { }
	fun onTurnedOn (): Unit { }
	fun onTurningOff (): Unit { }
	fun onTurningOn (): Unit { }
}
