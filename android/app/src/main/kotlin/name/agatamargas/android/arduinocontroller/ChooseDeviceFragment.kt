package name.agatamargas.android.arduinocontroller

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.fragment.app.Fragment
import androidx.room.Room

import kotlin.concurrent.thread

import name.agatamargas.android.arduinocontroller.databinding.FragmentChooseDeviceBinding
import name.agatamargas.android.arduinocontroller.vehicles.BluetoothVehicle

class ChooseDeviceFragment(private val parent: ControllerActivity) : Fragment() {
	private lateinit var binding: FragmentChooseDeviceBinding
	private val deviceInfoDao: DeviceInfoDao by lazy {
		Room.databaseBuilder(parent, AppDatabase::class.java, AppDatabase.databaseName).build().deviceInfoDao()
	}

	override fun onCreateView (inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
		binding = FragmentChooseDeviceBinding.inflate(inflater)
		parent.setTitle(R.string.choose_device_title)
		return binding.root
	}

	override fun onResume (): Unit {
		super.onResume()
		if (parent.bluetoothAdapter.isEnabled) {
			listDevices()
		}
	}

	internal fun listDevices (): Unit {
		listRememberedDevices()
		listPairedDevices()
	}

	private fun listRememberedDevices (): Unit {
		thread {
			// Database cannot be accessed on the UI thread...
			val devices = deviceInfoDao.getAllDevices()
			// ...while views can be used only by threads that created them
			parent.runOnUiThread {
				binding.rememberedDevicesList.adapter = AvailableDevicesAdapter(
					parent,
					devices,
					AvailableDevicesAdapter.OnItemClickListener { startController(parent.bluetoothAdapter.getRemoteDevice(it.mac)) },
					object : AvailableDevicesAdapter.OnMenuItemClickListener {
						override fun forget (device: DeviceInfo) = forgetDevice(device)
					}
				)
			}
		}
	}

	private fun forgetDevice (device: DeviceInfo): Unit {
		thread {
			deviceInfoDao.delete(device)
			parent.runOnUiThread {
				listRememberedDevices()
			}
		}
	}

	private fun listPairedDevices (): Unit {
		binding.pairedDevicesList.adapter = AvailableDevicesAdapter(
			parent,
			parent.bluetoothAdapter.bondedDevices.map { DeviceInfo(it) },
			AvailableDevicesAdapter.OnItemClickListener { startController(parent.bluetoothAdapter.getRemoteDevice(it.mac)) }
		)
	}

	private fun startController (device: BluetoothDevice): Unit {
		thread {
			deviceInfoDao.insert(device)
		}
		parent.currentFragment = ControllerFragment(parent, BluetoothVehicle(device))
	}
}
