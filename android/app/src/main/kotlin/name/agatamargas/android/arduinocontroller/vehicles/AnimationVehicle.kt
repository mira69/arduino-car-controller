package name.agatamargas.android.arduinocontroller.vehicles

import android.content.res.Resources
import android.graphics.drawable.AnimatedVectorDrawable
import android.widget.ImageView

import androidx.core.content.res.ResourcesCompat

import name.agatamargas.android.arduinocontroller.R

class AnimatedVehicle(private val vehicleView: ImageView, resources: Resources) : VehicleInterface {
	private val forwardBackground: AnimatedVectorDrawable =
		ResourcesCompat.getDrawable(resources, R.drawable.car_background_animation_forward, null) as AnimatedVectorDrawable
	private val backwardBackground: AnimatedVectorDrawable =
		ResourcesCompat.getDrawable(resources, R.drawable.car_background_animation_backward, null) as AnimatedVectorDrawable
	private val forwardRightBackground: AnimatedVectorDrawable =
		ResourcesCompat.getDrawable(resources, R.drawable.car_background_animation_forward_right, null) as AnimatedVectorDrawable
	private val forwardLeftBackground: AnimatedVectorDrawable =
		ResourcesCompat.getDrawable(resources, R.drawable.car_background_animation_forward_left, null) as AnimatedVectorDrawable
	private val value: VehicleMovement = VehicleMovement()

	override fun receive (): Int = value.encode()

	override fun send (flags: Int): Boolean {
		value.decode(flags)
		decode()
		return true
	}

	private fun decode() {
		when (value.direction) {
			VehicleMovement.FORWARD -> {
				val direction = if (value.turningDegrees > 10) value.turningDirection else VehicleMovement.STOP
				when (direction) {
					VehicleMovement.RIGHT -> setAnimation(forwardRightBackground)
					VehicleMovement.LEFT -> setAnimation(forwardLeftBackground)
					VehicleMovement.STOP -> setAnimation(forwardBackground)
				}
			}
			VehicleMovement.BACKWARD -> {
				val direction = if (value.turningDegrees > 10) value.turningDirection else VehicleMovement.STOP
				when (direction) {
					VehicleMovement.RIGHT -> setAnimation(forwardLeftBackground)
					VehicleMovement.LEFT -> setAnimation(forwardRightBackground)
					VehicleMovement.STOP -> setAnimation(backwardBackground)
				}
			}
			VehicleMovement.STOP -> (vehicleView.drawable as AnimatedVectorDrawable).stop()
		}
	}

	private fun setAnimation (drawable: AnimatedVectorDrawable) {
		if (vehicleView.drawable != drawable) {
			(vehicleView.drawable as AnimatedVectorDrawable).stop()
			vehicleView.setImageDrawable(drawable)
		}
		if (!(vehicleView.drawable as AnimatedVectorDrawable).isRunning()) {
			(vehicleView.drawable as AnimatedVectorDrawable).start()
		}
	}
}
