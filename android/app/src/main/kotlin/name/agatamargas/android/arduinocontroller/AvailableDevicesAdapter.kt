package name.agatamargas.android.arduinocontroller

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListPopupWindow
import android.widget.TextView

import androidx.recyclerview.widget.RecyclerView

import name.agatamargas.android.arduinocontroller.databinding.ItemBluetoothDeviceBinding

class AvailableDevicesAdapter(
	private val activity: Activity,
	private val devices: List<DeviceInfo>,
	private val listener: OnItemClickListener,
	_menuListener: OnMenuItemClickListener? = null
) : RecyclerView.Adapter<AvailableDevicesAdapter.ViewHolder>() {

	private val menuListener: OnMenuItemClickListener = _menuListener ?: object : OnMenuItemClickListener { }
	private val isMenuEnabled: Boolean = if (_menuListener == null) false else true

	override fun getItemCount (): Int
		= devices.size

	override fun onBindViewHolder (holder: ViewHolder, position: Int): Unit {
		holder.deviceName.text = devices[position].name
		holder.deviceMac.text = devices[position].mac
	}

	override fun onCreateViewHolder (parent: ViewGroup, viewType: Int): ViewHolder {
		return ViewHolder(ItemBluetoothDeviceBinding.inflate(LayoutInflater.from(parent.context)))
	}

	inner class ViewHolder(binding: ItemBluetoothDeviceBinding) : RecyclerView.ViewHolder(binding.root) {
		val deviceName: TextView = binding.deviceName
		val deviceMac: TextView = binding.deviceMac
		private val menu: ListPopupWindow = ListPopupWindow(activity)
		private val menuItems: Array<MenuItem> = arrayOf(
			MenuItem(
				activity.resources.getString(R.string.forget_device_menu_item_text),
				{ this@AvailableDevicesAdapter.menuListener.forget(getAdapterItem()) }
			)
		)

		init {
			if (isMenuEnabled) {
				menu.setAdapter(ArrayAdapter(activity, R.layout.popup_menu_item, menuItems))
				menu.anchorView = binding.root
				menu.isModal = true
				menu.setOnItemClickListener({ parent, _, position, _ ->
					(parent.getItemAtPosition(position) as? MenuItem)?.let { it.action() }
					menu.dismiss()
				})
				binding.root.setOnLongClickListener(View.OnLongClickListener {
					menu.show()
					false
				})
			}
			// For some reason the xml-set match_parent does not take effect
			binding.root.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
			binding.root.setOnClickListener(View.OnClickListener {
				this@AvailableDevicesAdapter.listener(getAdapterItem())
			})
		}

		private fun getAdapterItem(): DeviceInfo = this@AvailableDevicesAdapter.devices[this@ViewHolder.getBindingAdapterPosition()]
	}

	data class MenuItem(
		val name: String,
		val action: () -> Unit
	) {
		override fun toString (): String = name
	}

	fun interface OnItemClickListener {
		operator fun invoke(device: DeviceInfo)
	}
	interface OnMenuItemClickListener {
		fun forget(device: DeviceInfo) { }
	}
}
