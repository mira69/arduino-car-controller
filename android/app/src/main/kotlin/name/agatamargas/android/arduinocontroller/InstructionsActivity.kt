package name.agatamargas.android.arduinocontroller

import android.os.Bundle
import android.text.Html

import androidx.appcompat.app.AppCompatActivity

import name.agatamargas.android.arduinocontroller.databinding.ActivityInstructionsBinding

class InstructionsActivity : AppCompatActivity() {
	private val binding: ActivityInstructionsBinding by lazy {
		ActivityInstructionsBinding.inflate(layoutInflater)
	}

	override fun onCreate (savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(binding.root)
		setTitle(R.string.instructions_button_description)
		binding.instructionsField.loadDataWithBaseURL(null, getString(R.string.instructions_text), "text/html", "utf-8", null)
	}
}
