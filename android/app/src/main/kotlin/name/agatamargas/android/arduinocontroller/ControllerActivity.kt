package name.agatamargas.android.arduinocontroller

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle

import name.agatamargas.android.arduinocontroller.databinding.ActivityControllerBinding

class ControllerActivity : AppCompatActivity() {
	companion object {
		const val REQUEST_ENABLE_BT: Int = 0
	}

	private val binding: ActivityControllerBinding by lazy {
		ActivityControllerBinding.inflate(layoutInflater)
	}
	internal val bluetoothAdapter: BluetoothAdapter by lazy {
		(getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager).getAdapter()
	}
	// The field must be initialized and lateinit modifier
	// is not allowed on fields with custom getters or setters
	internal var currentFragment: Fragment = Fragment()
		set(value) {
			val valueType = value::class
			val fieldType = field::class
			// Do not reassign
			if (valueType == fieldType) {
				return
			}
			field = value
			supportFragmentManager.commit {
				setReorderingAllowed(true)
				setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
				replace(R.id.controller_frame, field)
			}
		}

	override fun onCreate (savedInstanceState: Bundle?): Unit {
		super.onCreate(savedInstanceState)
		setContentView(binding.root)
		currentFragment = ChooseDeviceFragment(this)

		// Receive broadcasts whenever Bluetooth is turned off
		// It must be turned on in this activity
		registerReceiver(
			BluetoothBroadcastReceiver(
				object : BluetoothBRListener {
					override fun onTurnedOff (): Unit {
						// React only if app is at least in resumed state
						// Otherwise turnOnBluetooth() will be caled from onStart()
						if (lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)) {
							turnOnBluetooth()
						}
					}
				}
			),
			IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
		)
	}

	override fun onStart (): Unit {
		super.onStart()
		if (!bluetoothAdapter.isEnabled) {
			turnOnBluetooth()
		}
	}

	override fun onBackPressed (): Unit {
		if (currentFragment is ControllerFragment) {
			currentFragment = ChooseDeviceFragment(this)
		}
		else {
			super.onBackPressed()
		}
	}

	override fun onActivityResult (requestCode: Int, resultCode: Int, data: Intent?): Unit {
		super.onActivityResult(requestCode, resultCode, data)
		when (requestCode) {
			REQUEST_ENABLE_BT -> when (resultCode) {
				RESULT_CANCELED -> bluetoothCanceled()
				RESULT_OK -> when (currentFragment) {
					is ChooseDeviceFragment -> (currentFragment as ChooseDeviceFragment).listDevices()
					is ControllerFragment -> (currentFragment as ControllerFragment).reconnect()
				}
			}
		}
	}

	private fun turnOnBluetooth (): Unit {
		// Ask for Bluetooth to be turned on
		startActivityForResult(Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), REQUEST_ENABLE_BT)
	}

	private fun bluetoothCanceled (): Unit {
		// Return to main screen
		finish()
	}
}
