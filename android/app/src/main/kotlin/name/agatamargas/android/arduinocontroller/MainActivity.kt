package name.agatamargas.android.arduinocontroller

import android.content.Intent
import android.os.Bundle
import android.view.View

import androidx.appcompat.app.AppCompatActivity

import java.util.Locale

import name.agatamargas.android.arduinocontroller.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
	companion object {
		const val preferencesName: String = "preferences"
		const val localeKey: String = "locale"
		const val localeDialogTag: String = "Languages dialog"
	}
	private val binding: ActivityMainBinding by lazy {
		ActivityMainBinding.inflate(layoutInflater)
	}

	override fun onCreate (savedInstanceState: Bundle?): Unit {
		// Preferred language must be set BEFORE calling super
		val prefs = getSharedPreferences(preferencesName, MODE_PRIVATE)
		val prefLocale = prefs.getString(localeKey, null)
		resources.updateConfiguration(
			resources.configuration.apply {
				setLocale(
					if (prefLocale != null) Locale(prefLocale)
					else Locale.getDefault()
				)
			},
			resources.displayMetrics
		)

		super.onCreate(savedInstanceState)
		setContentView(binding.root)

		// Show/hide menu if the settingsButton is clicked
		binding.settingsButton.setOnClickListener {
			if (binding.languageButton.isShown()) {
				binding.languageButton.hide()
				binding.instructionsButton.hide()
			}
			else {
				binding.instructionsButton.show()
				binding.languageButton.show()
			}
		}

		// chooseDeviceButton moves to the "Available devices" screen
		binding.chooseDeviceButton.setOnClickListener {
			startActivity(Intent(this, ControllerActivity::class.java))
		}

		binding.languageButton.setOnClickListener {
			LanguagesFragment().show(supportFragmentManager, localeDialogTag)
		}

		binding.instructionsButton.setOnClickListener {
			startActivity(Intent(this, InstructionsActivity::class.java))
		}
	}

	override fun onResume () {
		super.onResume()
		binding.languageButton.visibility = View.INVISIBLE
		binding.instructionsButton.visibility = View.INVISIBLE
	}
}
