package name.agatamargas.android.arduinocontroller

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice

import androidx.room.*

@Database(entities = [DeviceInfo::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
	abstract fun deviceInfoDao(): DeviceInfoDao

	class InvalidMacException : Exception()

	companion object {
		const val databaseName = "app-database"
	}
}

@Entity
data class DeviceInfo(
	@PrimaryKey val mac: String,
	var name: String
) {
	constructor (device: BluetoothDevice): this(device.address, device.name)

	init {
		if (!BluetoothAdapter.checkBluetoothAddress(mac)) {
			// This should never happen if this entity is used with BluetoothDevice
			throw AppDatabase.InvalidMacException()
		}
	}
}

@Dao
abstract class DeviceInfoDao {
	@Insert(onConflict = OnConflictStrategy.IGNORE)
	protected abstract fun insert(device: DeviceInfo)
	@Update
	abstract fun update(device: DeviceInfo)
	@Delete
	abstract fun delete(device: DeviceInfo)
	@Query("select * from deviceinfo")
	abstract fun getAllDevices(): List<DeviceInfo>
	@Query("select * from deviceinfo where mac = :mac")
	protected abstract fun getDeviceInfo(mac: String): DeviceInfo

	fun insert(device: BluetoothDevice) = insert(DeviceInfo(device))
	fun delete(device: BluetoothDevice) = delete(DeviceInfo(device))
	fun getDeviceInfo(device: BluetoothDevice) = getDeviceInfo(device.address)
}
