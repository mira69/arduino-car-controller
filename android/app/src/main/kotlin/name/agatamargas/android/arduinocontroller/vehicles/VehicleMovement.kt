package name.agatamargas.android.arduinocontroller.vehicles

class VehicleMovement {
	companion object {
		const val STOP: Int = 0
		const val FORWARD: Int = 1
		const val BACKWARD: Int = 2
		const val RIGHT: Int = 3
		const val LEFT: Int = 4

		private const val FORWARD_FLAG: Int = 0b10000000
		private const val LEFT_FLAG: Int = 0b01000000
	}

	// Possible values: FORWARD, BACKWARD, or STOP
	var direction: Int = STOP
		set(value) {
			if (value != FORWARD && value != BACKWARD) {
				field = STOP
			}
			else {
				field = value
			}
		}
	
	// Possible values: RIGHT, LEFT, or STOP if not turning
	var turningDirection: Int = STOP
		set(value) {
			if (value != RIGHT && value != LEFT) {
				field = STOP
			}
			else {
				field = value
			}
		}

	// Value from 0 to 90
	// 0 - no turn
	// 90 - maximal turn
	var turningDegrees: Int = 0
		set(value) {
			if (value < 0 || value > 90) {
				field = 0
			}
			else {
				field = value
			}
		}

	fun setTurning (direction: Int, degrees: Int) {
		turningDirection = direction
		turningDegrees = degrees
	}

	fun encode(): Int {
		var value = 0
		if (direction == STOP) {
			return value
		}
		if (turningDirection != STOP) {
			// 2-degrees precision is enough
			// Should be less than 64 if all data
			// is to be sent in one byte
			value = turningDegrees / 2
		}
		if (turningDirection == LEFT) {
			value = value or LEFT_FLAG
		}
		if (direction == FORWARD) {
			value = value or FORWARD_FLAG
		}
		else if (direction == BACKWARD && value == 0) {
			// This value is impossible to obtain otherwise
			value = 50
		}
		return value
	}

	fun decode (value: Int) {
		// Function parameters are read-only
		var value = value
		if (value == 0) {
			direction = STOP
			turningDirection = STOP
			turningDegrees = 0
			return
		}
		if (value == 50) {
			direction = BACKWARD
			turningDirection = STOP
			turningDegrees = 0
			return
		}
		if ((value and FORWARD_FLAG) != 0) {
			direction = FORWARD
			value = value - FORWARD_FLAG
		}
		else {
			direction = BACKWARD
		}
		if ((value and LEFT_FLAG) != 0) {
			turningDirection = LEFT
			value = value - LEFT_FLAG
		}
		else {
			turningDirection = RIGHT
		}
		if (value == 0) {
			turningDirection = STOP
			turningDegrees = 0
		}
		else {
			turningDegrees = value * 2
		}
	}
}
