package name.agatamargas.android.arduinocontroller.vehicles

interface VehicleInterface {
	// Send message to the vehicle, being an encoded value
	// from a VehicleMovement object.
	// Return value:
	// - true on success
	// - false on failure
	fun send (flags: Int): Boolean

	// Receive message (integer) from the vehicle.
	// As for now, the vehicle is expected to echo sent values.
	// Return value:
	// - non-negative on success
	// - -1 on failure
	fun receive (): Int
}
