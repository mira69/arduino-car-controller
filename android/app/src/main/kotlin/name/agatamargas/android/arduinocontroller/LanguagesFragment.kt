package name.agatamargas.android.arduinocontroller

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle

import androidx.fragment.app.DialogFragment

import java.util.Locale

class LanguagesFragment : DialogFragment() {

	override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
		return activity?.let {
			val prefs = it.getSharedPreferences(MainActivity.preferencesName, Context.MODE_PRIVATE)
			val prefLocaleName = prefs.getString(MainActivity.localeKey, null)
			val prefLocale =
				if (prefLocaleName != null) Locale(prefLocaleName)
				else Locale.getDefault()
			val languages = arrayOf(
				Locale.getDefault(),
				Locale.ENGLISH,
				Locale("pl")
			)
			val languageNames = languages.map { it.getDisplayName(prefLocale) }.toTypedArray()
			languageNames[0] = it.resources.getString(R.string.system_default_text)
			val builder = AlertDialog.Builder(it)
			builder.setTitle(R.string.language_button_description)
				.setSingleChoiceItems(
					languageNames,
					when (prefLocaleName) {
						null -> 0
						else -> {
							val idx = languageNames.indexOf(prefLocale.getDisplayName(prefLocale))
							if (idx >= 0) idx
							else 0
						}
					},
				DialogInterface.OnClickListener { _, which ->
					val prefLocale = when (which) {
						0 -> null
						else -> languages[which]
					}
					prefs.edit().putString(MainActivity.localeKey, prefLocale?.getLanguage()).apply()
					it.recreate()
				})
			builder.create()
		} ?: throw IllegalStateException("Activity cannot be null")
	}

}
