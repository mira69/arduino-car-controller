package name.agatamargas.android.arduinocontroller.vehicles

import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.os.Parcel
import android.os.Parcelable

import java.io.IOException
import java.util.UUID

class BluetoothVehicle(private val device: BluetoothDevice) : VehicleInterface {
	private val uuid: UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")
	private val socket: BluetoothSocket = device.createRfcommSocketToServiceRecord(uuid)

	// All of the methods (apart from close) are blocking
	// and thus shall not be called from the main, i.e. UI, thread.

	// Initiate connection with the vehicle. Must be called
	// before first send or recieve if they are to succeed.
	// Return value:
	// - true if succeeded or if connection was already
	//   established
	// - false otherwise
	fun connect (): Boolean {
		if (!socket.isConnected()) {
			try {
				socket.connect()
			} catch (e: IOException) {
				e.printStackTrace()
				return false
			}
		}
		return true
	}

	// Close the connection with the vehicle.
	// Return value:
	// - true if succeeded or if the connection is already closed
	// - false otherwise
	fun close (): Boolean {
		try {
			socket.close()
		} catch (e: IOException) {
			e.printStackTrace()
			return false
		}
		return true
	}

	override fun send (flags: Int): Boolean {
		try {
			socket.outputStream.write(flags)
		} catch (e: IOException) {
			e.printStackTrace()
			return false
		}
		return true
	}

	override fun receive (): Int {
		var result: Int = -1
		try {
			result = socket.inputStream.read()
		} catch (e: IOException) {
			e.printStackTrace()
			// result is -1
		}
		return result
	}
}
