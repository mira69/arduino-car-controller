package name.agatamargas.android.arduinocontroller

import android.annotation.SuppressLint
import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup

import androidx.fragment.app.Fragment

import java.lang.Math
import java.util.concurrent.locks.ReentrantLock

import kotlin.concurrent.thread

import name.agatamargas.android.arduinocontroller.databinding.FragmentControllerBinding
import name.agatamargas.android.arduinocontroller.vehicles.AnimatedVehicle
import name.agatamargas.android.arduinocontroller.vehicles.BluetoothVehicle
import name.agatamargas.android.arduinocontroller.vehicles.VehicleMovement

class ControllerFragment(
	private val parent: ControllerActivity,
	private val vehicle: BluetoothVehicle
) : Fragment(), SensorEventListener {
	private lateinit var binding: FragmentControllerBinding
	private val movement = VehicleMovement()
	private val movementLock = ReentrantLock()

	private val sensorManager: SensorManager by lazy {
		parent.getSystemService(Context.SENSOR_SERVICE) as SensorManager
	}
	private val accelerometer: Sensor? by lazy {
		sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
	}
	private val magnetometer: Sensor? by lazy {
		sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD)
	}
	private val accelerometerReading = FloatArray(3)
	private val magnetometerReading = FloatArray(3)

	private val animation: AnimatedVehicle by lazy {
		AnimatedVehicle(binding.carBackground, parent.resources)
	}

	override fun onCreateView (inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
		binding = FragmentControllerBinding.inflate(inflater)
		return binding.root
	}

	override fun onViewCreated (view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		parent.setTitle(R.string.app_name)
	}

	override fun onAccuracyChanged (sensor: Sensor, accuracy: Int) { }

	override fun onSensorChanged (event: SensorEvent) {
		when (event.sensor.type) {
			Sensor.TYPE_ACCELEROMETER -> {
				event.values.copyInto(accelerometerReading)
			}
			Sensor.TYPE_MAGNETIC_FIELD -> {
				event.values.copyInto(magnetometerReading)
			}
		}
		val rotationMatrix = FloatArray(9)
		SensorManager.getRotationMatrix(rotationMatrix, null, accelerometerReading, magnetometerReading)
		val orientationAngles = FloatArray(3)
		SensorManager.getOrientation(rotationMatrix, orientationAngles)
		// Angles are given in radians, we want degrees
		val roll = ((180 * orientationAngles[1]) / Math.PI).toInt()
		if (movementLock.tryLock()) {
			if (roll > 0) {
				movement.setTurning(VehicleMovement.LEFT, roll)
			}
			else if (roll < 0) {
				movement.setTurning(VehicleMovement.RIGHT, -roll)
			}
			else {
				movement.setTurning(VehicleMovement.STOP, 0)
			}
			movementLock.unlock()
		}
	}

	override fun onResume() {
		super.onResume()
		reconnect()
		accelerometer?.also {
			sensorManager.registerListener(this, it, SensorManager.SENSOR_DELAY_NORMAL)
		}
		magnetometer?.also {
			sensorManager.registerListener(this, it, SensorManager.SENSOR_DELAY_NORMAL)
		}
	}

	override fun onPause() {
		super.onPause()
		sensorManager.unregisterListener(this)
	}

	fun reconnect() {
		// connect() may be blocking and thus must be run on a different
		// thread. However, we want to continue from the main thread.
		thread {
			when (vehicle.connect()) {
				true -> parent.runOnUiThread {
					onConnectionEstablished()
				}
				false -> parent.runOnUiThread {
					errorAndFinish(R.string.could_not_connect)
				}
			}
		}
	}

	override fun onDestroy (): Unit {
		super.onDestroy()
		// The result is not important at this point.
		vehicle.close()
	}
	
	private fun onConnectionEstablished (): Unit {
		Toast.makeText(parent, R.string.connected, Toast.LENGTH_SHORT).show()
		binding.connectionProgressBar.visibility = View.GONE
		binding.carAnimationView.visibility = View.VISIBLE
		binding.backwardButton.apply {
			visibility = View.VISIBLE
			setOnTouchListener @SuppressLint("ClickableViewAccessibility") { _, event ->
				when (event.getActionMasked()) {
					MotionEvent.ACTION_DOWN -> {
						if (movementLock.tryLock()) {
							if (movement.direction == VehicleMovement.STOP) {
								movement.direction = VehicleMovement.BACKWARD
							}
							movementLock.unlock()
						}
						sendFlags()
						true
					}
					MotionEvent.ACTION_MOVE -> {
						sendFlags()
						true
					}
					MotionEvent.ACTION_UP -> {
						if (movementLock.tryLock()) {
							if (movement.direction == VehicleMovement.BACKWARD) {
								movement.direction = VehicleMovement.STOP
							}
							movementLock.unlock()
						}
						sendFlags()
						true
					}
					else -> false
				}
			}
		}
		binding.forwardButton.apply {
			visibility = View.VISIBLE
			setOnTouchListener @SuppressLint("ClickableViewAccessibility") { _, event ->
				when (event.getActionMasked()) {
					MotionEvent.ACTION_DOWN -> {
						if (movementLock.tryLock()) {
							if (movement.direction == VehicleMovement.STOP) {
								movement.direction = VehicleMovement.FORWARD
							}
							movementLock.unlock()
						}
						sendFlags()
						true
					}
					MotionEvent.ACTION_MOVE -> {
						sendFlags()
						true
					}
					MotionEvent.ACTION_UP -> {
						if (movementLock.tryLock()) {
							if (movement.direction == VehicleMovement.FORWARD) {
								movement.direction = VehicleMovement.STOP
							}
							movementLock.unlock()
						}
						sendFlags()
						true
					}
					else -> false
				}
			}
		}
		binding.carAnimationView.setOnClickListener {
			if (binding.blackOverlay.visibility == View.VISIBLE) {
				binding.blackOverlay.visibility = View.INVISIBLE
				parent.getSupportActionBar()?.show()
				parent.window.decorView.systemUiVisibility = 0
			}
			else {
				binding.blackOverlay.visibility = View.VISIBLE
				parent.getSupportActionBar()?.hide()
				parent.window.decorView.systemUiVisibility =
					View.SYSTEM_UI_FLAG_FULLSCREEN or
					View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or
					View.SYSTEM_UI_FLAG_IMMERSIVE
			}
		}
	}

	private fun sendFlags (): Unit {
		thread {
			if (vehicle.send(movement.encode())) {
				val result = vehicle.receive()
				parent.runOnUiThread {
					animation.send(result)
				}
			}
			else {
				reconnect()
			}
		}
	}

	private fun errorAndFinish (msgId: Int): Unit {
		Toast.makeText(parent, msgId, Toast.LENGTH_SHORT).show()
		parent.currentFragment = ChooseDeviceFragment(parent)
	}
}
