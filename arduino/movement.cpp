#include "movement.h"

// These two functions are the same as in kotlin code.

int encode (Movement m) {
	int value = 0;
	if (m.direction == Direction::STOP) {
		return value;
	}
	if (m.turningDirection != Direction::STOP) {
		value = m.turningDegrees >> 1;
	}
	if (m.turningDirection == Direction::LEFT) {
		value = value | LEFT_FLAG;
	}
	if (m.direction == Direction::FORWARD) {
		value = value | FORWARD_FLAG;
	}
	else if (m.direction == Direction::BACKWARD && value == 0) {
		value = 50;
	}
	return value;
}

Movement decode (int value) {
	Movement m = Movement();
	if (value == 0) {
		m.direction = Direction::STOP;
		m.turningDirection = Direction::STOP;
		m.turningDegrees = 0;
		return m;
	}
	if (value == 50) {
		m.direction = Direction::BACKWARD;
		m.turningDirection = Direction::STOP;
		m.turningDegrees = 0;
		return m;
	}
	if (value & FORWARD_FLAG) {
		m.direction = Direction::FORWARD;
		value = value - FORWARD_FLAG;
	}
	else {
		m.direction = Direction::BACKWARD;
	}
	if (value & LEFT_FLAG) {
		m.turningDirection = Direction::LEFT;
		value = value - LEFT_FLAG;
	}
	else {
		m.turningDirection = Direction::RIGHT;
	}
	if (value == 0) {
		m.turningDirection = Direction::STOP;
		m.turningDegrees = 0;
	}
	else {
		m.turningDegrees = value << 1;
	}
	return m;
}
