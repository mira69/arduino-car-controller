#include <Servo.h>
#include "movement.h"

constexpr int serialBaudRate = 9600;
constexpr int rightServoPin = 3;
constexpr int leftServoPin = 5;
// These are the values where my servos don't move.
constexpr int leftServoZero = 96;
constexpr int rightServoZero = 96;
constexpr int leftServoMax = 180;
constexpr int leftServoMin = 0;
constexpr int rightServoMax = leftServoMin;
constexpr int rightServoMin = leftServoMax;

int incomingData;
Movement decodedData;
Servo leftServo, rightServo;

void setup () {
    Serial.begin(serialBaudRate);
    leftServo.attach(leftServoPin);
    rightServo.attach(rightServoPin);
    rightServo.write(rightServoZero);
}

void loop () {
    if (Serial.available()) {
        incomingData = Serial.read();
        decodedData = decode(incomingData);
        switch (decodedData.direction) {
            case Direction::STOP:
                leftServo.write(leftServoZero);
                rightServo.write(rightServoZero);
                break;
            case Direction::FORWARD:
                switch (decodedData.turningDirection) {
                    case Direction::STOP:
                        leftServo.write(leftServoMax);
                        rightServo.write(rightServoMax);
                        break;
                    case Direction::RIGHT:
                        leftServo.write(leftServoMax);
                        rightServo.write(abs(rightServoMax - (decodedData.turningDegrees << 1)));
                        break;
                    case Direction::LEFT:
                        leftServo.write(abs(leftServoMax - (decodedData.turningDegrees << 1)));
                        rightServo.write(rightServoMax);
                        break;
                }
                break;
            case Direction::BACKWARD:
                switch (decodedData.turningDirection) {
                    case Direction::STOP:
                        leftServo.write(leftServoMin);
                        rightServo.write(rightServoMin);
                        break;
                    case Direction::RIGHT:
                        leftServo.write(leftServoMin);
                        rightServo.write(abs(rightServoMin - (decodedData.turningDegrees << 1)));
                        break;
                    case Direction::LEFT:
                        leftServo.write(abs(leftServoMin - (decodedData.turningDegrees << 1)));
                        rightServo.write(rightServoMin);
                        break;
                }
                break;
        }
        Serial.write(incomingData);
    }
}
