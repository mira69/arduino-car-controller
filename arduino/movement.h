#ifndef MOVEMENT_H_INCLUDED
#define MOVEMENT_H_INCLUDED

enum class Direction {
	STOP, FORWARD, BACKWARD, RIGHT, LEFT
};

constexpr int FORWARD_FLAG = 0b10000000;
constexpr int LEFT_FLAG = 0b01000000;

struct Movement {
	Direction direction;
	Direction turningDirection;
	int turningDegrees;

	Movement(): Movement(Direction::STOP, Direction::STOP, 0) { }

	Movement(Direction dir, Direction tdir, int tdeg):
		direction(dir),
		turningDirection(tdir),
		turningDegrees(tdeg)
	{ }
};

int encode (Movement m);
Movement decode (int value);

#endif /* MOVEMENT_H_INCLUDED */
