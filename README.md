# Arduino Car Controller

This project has two parts:
- Android app for controlling an Arduino vehicle and
- corresponding Arduino code.

Used external libraries are listed in the dependencies section in the [build.gradle](android/app/build.gradle) file.
